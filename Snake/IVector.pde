class IVector {
  int x;
  int y;
  
  IVector(int x, int y) {
    this.x = x;
    this.y = y;
  }
  
  IVector(int max) {
    x = (int) random(max);
    y = (int) random(max);
  }
}
