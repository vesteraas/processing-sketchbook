int px = 16;
IVector apple = new IVector(24);
IVector head = new IVector(12, 12);
ArrayList<IVector> body = new ArrayList<IVector>();
IVector direction = new IVector(1, 0);
int frameLength = 5;
int nextFrame = 0;
int points = 0;
int grow = 1;

void setup() {
  frameRate(30);
  size(24*px, 24*px);
}

void draw() {
  nextFrame++;
  if (nextFrame >= frameLength) {
    nextFrame = 0;
    update();
  }
  background(255);
  fill(0);
  text(points, 15, 15);
  rect(head.x*px, head.y*px, px, px);
  for (IVector v : body) {
    rect(v.x*px, v.y*px, px, px);
  }
  fill(255, 0, 0);
  rect(apple.x*px, apple.y*px, px, px);
}

void update() {
  body.add(head);
  if (grow > 0) {
    grow--;
  } else {
    body.remove(0);
  }
  head = new IVector(head.x+direction.x, head.y+direction.y);
  if (head.x == apple.x && head.y == apple.y) {
    points++;
    grow += 2;
    apple = new IVector(24);
  }
}

void keyPressed() {
  if (key == 'w') {
    direction = new IVector(0, -1);
  }
  if (key == 's') {
    direction = new IVector(0, 1);
  }
  if (key == 'a') {
    direction = new IVector(-1, 0);
  }
  if (key == 'd') {
    direction = new IVector(1, 0);
  }
}
