Particle[] particles = new Particle[10000];

void setup() {
  size(800, 600);
  for(int i = 0; i < particles.length; i++) {
    particles[i] = new Particle(random(width), random(height), random(20)-10, random(20)-10);
  }
  fill(255);
  stroke(255);
  noSmooth();
}

void draw() {
  background(0);
  for(Particle p : particles) {
    p.ox = p.x;
    p.oy = p.y;
    p.x += p.vx;
    p.y += p.vy;
    
    if (p.x < 0) {
      p.x = 0;
      p.vx *= -1;
    }
    
    if (p.y < 0) {
      p.y = 0;
      p.vy *= -1;
    }
    
    if (p.x > width) {
      p.x = width;
      p.vx *= -1;
    }
    
    if (p.y > height) {
      p.y = height;
      p.vy *= -1;
    }
    
    p.vx *= 0.98;
    p.vy *= 0.98;
    
    line(p.ox, p.oy, p.x, p.y);
  }
}

void mousePressed() {
  float a = 5;
  for (Particle p : particles) {
    float distance = dist(p.x, p.y, mouseX, mouseY);
    p.vx -= a / distance * (p.x - mouseX);
    p.vy -= a / distance * (p.y - mouseY);
  }
}
