class Particle {
  float x;
  float y;
  float ox = 0.0;
  float oy = 0.0;
  float vx;
  float vy;
  
  Particle(float x, float y, float vx, float vy) {
    this.x = x;
    this.y = y;
    this.vx = vx;
    this.vy = vy;
  }
}
