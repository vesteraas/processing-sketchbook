float time = 0;
float x = 200;
float y = 200;

void setup() {
  size(500, 500);
}

void draw() {
  background(0);
  if (time < 50) time++;
  float s = easeOutBounce(time, 0, 200, 50);
  text(time/50, 20, 40);
  text(s, 20, 20);
  fill(255);
  ellipse(x, y, s, s);
}

  float easeInCubic(float time, float begin, float change, float duration) {
    time /= duration;
    return change * time*time*time + begin;
  }
  float easeOutCubic(float time, float begin, float change, float duration) {
    time /= duration;
    time--;
    return change * (time*time*time + 1) + begin;
  }
  float easeInOutCubic(float time, float begin, float change, float duration) {
    if (time < duration / 2) return easeInCubic(time, begin, change/2, duration/2);
    return easeOutCubic(time-duration/2, begin + change/2, change/2, duration/2);
  }
  float easeOutBounce(float time, float begin, float change, float duration) {
    if (time < 2 * duration / 6)   return easeInCubic(time, begin, change, duration/3);
    if (time < 3 * duration / 6)   return easeOutCubic(time-duration/3, begin+change, -(begin+change)/3, duration/6);
    if (time < 4 * duration / 6)   return easeInCubic(time-3*duration/6, begin + 2*change/3, change/3, duration/6);
    if (time < 9 * duration / 12)  return easeOutCubic(time-8*duration/12, begin+change, -(begin+change)/6, duration/12);
    if (time < 10 * duration / 12) return easeInCubic(time-9*duration/12, begin + 5*change/6, change/6, duration/12);
    if (time < 11 * duration / 12) return easeOutCubic(time-10*duration/12, begin+change, -(begin+change)/12, duration/12);
                                   return easeInCubic(time-11*duration/12, begin + 11*change/12, change/12, duration/12);
  }
