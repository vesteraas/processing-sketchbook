import processing.serial.*;

Serial myPort;

boolean cButton = false;
boolean zButton = false;
int joyX = 0;
int joyY = 0;
int accelX = 0;
int accelY = 0;
int accelZ = 0;

PVector joyO;
PVector accelO;

void setup() {
  size(800, 800);
  println(Serial.list());
  myPort = new Serial(this, Serial.list()[4], 19200);
  myPort.bufferUntil('\n');
  joyO = new PVector(width/3, height/2);
  accelO = new PVector((width/3)*2, height/2);
}

void draw() {
  background(255);
  fill(0);
  if (cButton) ellipse(100, 100, 50, 50);
  if (zButton) ellipse(200, 100, 50, 50);
  line(joyO.x, joyO.y, joyO.x+joyX, joyO.y+joyY);
  line(accelO.x, accelO.y, accelO.x+accelX, accelO.y);
  line(accelO.x, accelO.y, accelO.x, accelO.y+accelY);
  line(accelO.x, accelO.y, accelO.x+accelZ, accelO.y+accelZ);
}

void serialEvent(Serial myPort) {
  String inString = myPort.readStringUntil('\n');
  if (inString != null && !("".equals(inString.trim()))) {
    String[] data = inString.trim().split(" ");
    if(data.length < 7) return;
    joyX = int(data[0])-138;
    joyY = int(data[1])-128;
    accelX = int(data[2])-512;
    accelY = int(data[3])-512;
    accelZ = int(data[4])-512;
    zButton = boolean(int(data[5]));
    cButton = boolean(int(data[6]));
  }
}

