void setup() {
    frameRate(30);
    size(800, 600);
}

void draw() {
    background(0);
    int spacing = 40;
    for (int x = 0; x < width + spacing; x += spacing) {
        for (int y = 0; y < height + spacing; y += spacing) {
            int distance = (int) dist(x, y, mouseX, mouseY);
            int r = x*255/width;
            int g = y*255/height;
            int b = 255 - (x*255/width + y*255/height) / 2;
            fill(r, g, b);
            noStroke();
            ellipse(x, y, distance / 10, distance / 10);
        }
    }
}
