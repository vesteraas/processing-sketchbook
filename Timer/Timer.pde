int s = 60;
int t = 0;
int curs = s;

void setup() {
  frameRate(10);
  size(displayWidth, displayHeight);
  textSize(400);
  textAlign(RIGHT);
}

void draw() {
  background(0);
  t -= 1;
  if (t < 0) {
    t = 9;
    curs--;
  }
  if (curs < 10) {
    background(90, 0, 0);
  }
  if (curs < 0) {
    background(180, 0, 0);
    curs = 0;
    t = 0;
  }
  text(curs + ":" + t, width-200, height/2+150);
}

void mousePressed() {
  curs = s;
  t = 0;
}
