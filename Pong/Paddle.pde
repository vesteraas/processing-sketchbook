class Paddle {
  float x;
  float y;
  int w = 100;
  int h = 20;
  
  boolean goingLeft = false;
  boolean goingRight = false;
  
  Paddle(float x, float y) {
    this.x = x;
    this.y = y;
  }
  
  void draw() {
    if (goingRight) {
      x += 5;
    }
    if (goingLeft) {
      x -= 5;
    }
    rect(x-w/2, y, w, h);
  }
}
