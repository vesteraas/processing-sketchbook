class Ball {
  float x;
  float y;
  float xv;
  float yv;
  float radius = 20;
  
  Ball(float x, float y) {
    this.x = x;
    this.y = y;
    xv = random(6)-3;
    yv = 3 * (random(2) > 1 ? 1 : -1);
  }
  
  void draw() {
    ellipse(x, y, radius*2, radius*2);
  }
}
