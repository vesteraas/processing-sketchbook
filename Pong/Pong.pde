Ball ball;
Paddle paddleA, paddleB;

void setup() {
  size(800, 800);
  ball = new Ball(width/2, height/2); 
  paddleA = new Paddle(width/2, 20);
  paddleB = new Paddle(width/2, height-40);
}

void draw() {
  background(0);
  fill(255);
  stroke(255);
  
  line(0, height/2, width, height/2);
  
  ball.x += ball.xv;
  ball.y += ball.yv;
  
  if (ball.y > height) {
    ball = new Ball(width/2, height/2);
  }
  if (ball.y < 0) {
    ball = new Ball(width/2, height/2);
  }
  if (ball.x - ball.radius < 0) {
    if (ball.xv < 0) ball.xv *= -1;
  }
  if (ball.x + ball.radius > width) {
    if (ball.xv > 0) ball.xv *= -1;
  }
  
  if (ball.y - ball.radius < paddleA.y+paddleA.h
      && ball.x > paddleA.x - paddleA.w/2
      && ball.x < paddleA.x + paddleA.w/2) {
    if (ball.yv < 0) ball.yv *= -1.1;
  }
  
  if (ball.y + ball.radius > paddleB.y
      && ball.x > paddleB.x - paddleB.w/2
      && ball.x < paddleB.x + paddleB.w/2) {
    if (ball.yv > 0) ball.yv *= -1.1;
  }
  
  ball.draw();
  paddleA.draw();
  paddleB.draw();
}

void keyPressed() {
  if (key == 'a') {
    paddleA.goingLeft = true;
  }
  if (key == 'd') {
    paddleA.goingRight = true;
  }
  if (key == CODED) {
    if (keyCode == LEFT) {
      paddleB.goingLeft = true;
    }
    if (keyCode == RIGHT) {
      paddleB.goingRight = true;
    }
  }
}

void keyReleased() {
  if (key == 'a') {
    paddleA.goingLeft = false;
  }
  if (key == 'd') {
    paddleA.goingRight = false;
  }
  if (key == CODED) {
    if (keyCode == LEFT) {
      paddleB.goingLeft = false;
    }
    if (keyCode == RIGHT) {
      paddleB.goingRight = false;
    }
  }
}
