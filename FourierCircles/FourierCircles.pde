ArrayList<RotatingLine> lines = new ArrayList<RotatingLine>();
float t = 0;

float px = 0;
float py = 0;

boolean drawPath = false;
boolean drawLastSegment = true;
boolean slowErase = false;

void setup() {
  size(500, 500);
  
  lines.add(new RotatingLine(60, 0.5));
  lines.add(new RotatingLine(60, 8));
  lines.add(new RotatingLine(60, 8));
}

void draw() {
  if(slowErase) {
    fill(255, 5);
    noStroke();
    rect(0, 0, width, height);
    stroke(0);
  }
  t += 0.03;
  
  fill(0);
  
  float lastX = width/2;
  float lastY = height/2;
  for(int i = 0; i < lines.size(); i++) {
    RotatingLine line = lines.get(i);
    
    float thisX = lastX + cos(t/line.period)*line.length;
    float thisY = lastY - sin(t/line.period)*line.length;
    
    if(drawLastSegment && i == lines.size() - 1) {
      line(lastX, lastY, thisX, thisY);
    }
    
    lastX = thisX;
    lastY = thisY;
  }
  
  if(drawPath && px != 0) {
    line(px, py, lastX, lastY);
  }
  px = lastX;
  py = lastY;
}

class RotatingLine {
  float period;
  int length;
  
  RotatingLine(int length, float period) {
    this.period = period;
    this.length = length;
  }
  
  RotatingLine(int length) {
    this(length, length / 100.0);
  }
}
