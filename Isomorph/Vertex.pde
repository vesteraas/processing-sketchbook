class Vertex {
  Node a;
  Node b;
  
  boolean marked = false;
  
  Vertex(Node a, Node b) {
    this.a = a;
    this.b = b;
  }
}
