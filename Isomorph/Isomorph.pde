ArrayList<Node> nodes = new ArrayList<Node>();
ArrayList<Vertex> vertices = new ArrayList<Vertex>();

int area = 400;
int rad = 20;
int margin = 20;

Node drawFrom = null;
Node holding = null;
boolean untangle = false;

void setup() {
  size(700, 700);
  for (int i = 0; i < 20; i++) {
    nodes.add(new Node(random(area), random(area)));
  }
  for (int i = 0; i < 28; i++) {
    Node a, b;
    boolean identical;
    do {
      identical = false;
      a = nodes.get((int) random(nodes.size()));
      b = nodes.get((int) random(nodes.size()));
      for (Vertex v : vertices) {
        if ((v.a == a && v.b == b) || (v.a == b && v.b == a)) {
          identical = true;
        }
      }
    } 
    while (a == b || identical == true);
    vertices.add(new Vertex(a, b));
  }
}

void draw() {
  background(255);

  if (untangle) untangle();

  if (mousePressed) {
    if (holding == null) {
      for (Node n : nodes) {
        if (dist(mouseX, mouseY, n.x, n.y) < rad) {
          holding = n;
        }
      }
    }
    if (holding != null) {
      holding.x = mouseX;
      holding.y = mouseY;
    }
  }

  strokeWeight(4);
  for (Vertex v : vertices) {
    stroke(0);
    if (v.marked) stroke(255, 0, 0);
    line(v.a.x, v.a.y, v.b.x, v.b.y);
  }
  if (drawFrom != null) {
    line(drawFrom.x, drawFrom.y, mouseX, mouseY);
  }

  stroke(0);
  strokeWeight(1);
  fill(128, 128, 0);
  for (Node n : nodes) {
    ellipse(n.x, n.y, 2 * rad, 2 * rad);
  }
}

void untangle() {
  for (Node n : nodes) {
    if (n.x - rad - margin < 0) n.x++;
    if (n.y - rad - margin < 0) n.y++;
    if (n.x + rad + margin > width) n.x--;
    if (n.y + rad + margin > height) n.y--;

    for (Node m : nodes) {
      if (n != m && n.dist(m) < 2 * rad + margin) {
        n.x += (n.x-m.x)*0.1;
        n.y += (n.y-m.y)*0.1;
      }
    }

    for (Vertex v : vertices) {
      if (v.a == n || v.b == n) continue;
      PVector cp = closestPointOnLine(n, v.a, v.b);
      if (n.dist(cp) < rad + margin) {
        PVector mv = new PVector((n.x-cp.x), (n.y-cp.y));
        mv.normalize();
        n.add(mv);
        n.add(mv);
        v.a.sub(mv);
        v.b.sub(mv);
      }
    }
  }
}

void keyPressed() {
  if (key == 'n') { // Draw node
    nodes.add(new Node(mouseX, mouseY));
  }
  if (key == 'v') { // Draw vertex
    Node t = null;
    for (Node n : nodes) {
      if (dist(mouseX, mouseY, n.x, n.y) < rad) t = n;
    }
    if (t != null) {
      if (drawFrom == null) {
        drawFrom = t;
      } 
      else {
        vertices.add(new Vertex(drawFrom, t));
        drawFrom = null;
      }
    }
  }
  if (key == 'u') { // Untangle
    untangle = !untangle;
  }
  if (key == 'm') { // Mark vertex
    for (Vertex v : vertices) {
      PVector mouse = new PVector(mouseX, mouseY);
      PVector closest = closestPointOnLine(mouse, v.a, v.b);
      if (mouse.dist(closest) < 10) {
        v.marked = !v.marked;
      }
    }
  }
  if (key == 'r') { // Reset marks
    for (Vertex v : vertices) {
      v.marked = false;
    }
  }
}

void mouseReleased() {
  holding = null;
}

PVector closestPointOnLine(PVector p, PVector l1, PVector l2) {
  if (l1.dist(p) > l1.dist(l2)) return l2.get();
  if (l2.dist(p) > l2.dist(l1)) return l1.get();

  float A1 = l2.y - l1.y;
  float B1 = l1.x - l2.x;
  float C1 = A1*l1.x + B1*l1.y;
  float C2 = -B1*p.x + A1*p.y;
  float det = A1*A1 - -B1*B1;

  if (det == 0) return p.get();
  float cx = (A1*C1 - B1*C2) / det;
  float cy = (A1*C2 - -B1*C1) / det;
  return new PVector(cx, cy);
}

